﻿using Microsoft.Extensions.DependencyInjection;
using PucElevador.Dominio.Interfaces;
using PucElevador.Dominio.Interfaces.Data;
using PucElevador.Dominio.Servicos;
using PucElevador.Infra.Data;
using PucElevador.Infra.Data.Repositorio;
using System;
using System.Collections.Generic;
using System.Text;

namespace PucElevador.Infra.Ioc
{
    public class InjetorDeDependencia
    {
        public static void RegistrarServicos(IServiceCollection services)
        {
            //Domínio
            services.AddScoped<IElevadorService, ElevadorService>();
            services.AddScoped<ILeitorArquivosData, LeitorArquivosData>();
            services.AddScoped<IElevadorRepositorio, ElevadorRepositorio>();
        }
    }
}
