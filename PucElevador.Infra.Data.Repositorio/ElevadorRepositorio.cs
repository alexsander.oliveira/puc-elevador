﻿using PucElevador.Dominio.Interfaces.Data;
using PucElevador.Dominio.Modelos;
using System.Collections.Generic;
using System.Linq;

namespace PucElevador.Infra.Data.Repositorio
{
    public class ElevadorRepositorio : IElevadorRepositorio
    {
        private readonly ILeitorArquivosData _leitorArquivosData;
        private readonly List<DadoUsuarioElevador> _dadosUsuariosElevador;
        public ElevadorRepositorio(ILeitorArquivosData leitorArquivosData)
        {
            _leitorArquivosData = leitorArquivosData;
            _dadosUsuariosElevador = _leitorArquivosData.LerArquivo();
        }

        public List<int> ObterAndarMenosUtilizados(int quantidade)
        {
            var andares = _dadosUsuariosElevador.GroupBy(x => x.Andar)
                        .Select(group => new {
                            Andar = group.Key,
                            Count = group.Count()
                        })
                        .OrderBy(x => x.Count)
                        .Take(quantidade)
                        .Select(x => x.Andar)
                        .ToList();

            return andares;
        }

        public List<char> ObterElevadorMaisFrequentado(int quantidade)
        {
            var andares = _dadosUsuariosElevador.GroupBy(x => x.Elevador)
                       .Select(group => new {
                           Elevador = group.Key,
                           Count = group.Count()
                       })
                       .OrderByDescending(x => x.Count)
                       .Take(quantidade)
                       .Select(x => x.Elevador)
                       .ToList();

            return andares;
        }

        public List<char> ObterElevadorMenosFrequentado(int quantidade)
        {
            var elevadores = _dadosUsuariosElevador.GroupBy(x => x.Elevador)
                       .Select(group => new {
                           Elevador = group.Key,
                           Count = group.Count()
                       })
                       .OrderBy(x => x.Count)
                       .Take(quantidade)
                       .Select(x => x.Elevador)
                       .ToList();

            return elevadores;
        }

        public List<char> ObterPeriodoDeMaiorFluxoDoMaisFrequentado(char elevador, int quantidade)
        {
            var turnos = _dadosUsuariosElevador.Select(x => x).Where(x => x.Elevador == elevador)
                       .GroupBy(x => x.Turno)
                       .Select(group => new {
                           Turno = group.Key,
                           Count = group.Count()
                       })
                       .OrderByDescending(x => x.Count)
                       .Select(x => x.Turno)
                       .Take(quantidade)
                       .ToList();

            return turnos;
        }

        public List<char> ObterPeriodoDeMenorFluxoDoMenosFrequentado(char elevador, int quantidade)
        {
            var turnos = _dadosUsuariosElevador.Select(x => x).Where(x => x.Elevador == elevador)
                       .GroupBy(x => x.Turno)
                       .Select(group => new {
                           Turno = group.Key,
                           Count = group.Count()
                       })
                       .OrderBy(x => x.Count)
                       .Select(x => x.Turno)
                       .Take(quantidade)
                       .ToList();

            return turnos;
        }

        public List<char> ObterPeriodoDeMaiorUtilizacao(int quantidade)
        {
            var turnos = _dadosUsuariosElevador
                       .GroupBy(x => x.Turno)
                       .Select(group => new {
                           Turno = group.Key,
                           Count = group.Count()
                       })
                       .OrderByDescending(x => x.Count)
                       .Select(x => x.Turno)
                       .Take(quantidade)
                       .ToList();

            return turnos;
        }

        public float ObterPercentualDeUsoElevador(char elevador)
        {
            var total = _dadosUsuariosElevador.Count();

            var totalElevador = _dadosUsuariosElevador.Count(x => x.Elevador == elevador);

            var totalGeral = 100 / (total / totalElevador); 

            return totalGeral;
        }
    }
}
