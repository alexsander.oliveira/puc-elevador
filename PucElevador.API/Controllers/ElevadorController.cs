﻿using Microsoft.AspNetCore.Mvc;
using PucElevador.Dominio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PucElevador.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ElevadorController : ControllerBase
    {
        private readonly IElevadorService _elevadorService;

        public ElevadorController(IElevadorService elevadorService)
        {
            _elevadorService = elevadorService;
        }

        [HttpGet]
        [Route("MaisFrequentado")]
        public List<char> ObterElevadorMaisFrequentado()
        {
            return _elevadorService.ElevadorMaisFrequentado();
        }

        [HttpGet]
        [Route("MenosFrequentado")]
        public List<char> ObterElevadorMenosFrequentado()
        {
            return _elevadorService.ElevadorMenosFrequentado();
        }

        [HttpGet]
        [Route("PeriodoMaiorFluxoMaisFrequentado")]
        public List<char> ObterPeriodoMaiorFluxoMaisFrequentado()
        {
            return _elevadorService.PeriodoMaiorFluxoElevadorMaisFrequentado();
        }

        [HttpGet]
        [Route("PeriodoMenorFluxoMenosFrequentado")]
        public List<char> ObterPeriodoMenorFluxoMenosFrequentado()
        {
            return _elevadorService.PeriodoMenorFluxoElevadorMenosFrequentado();
        }

        [HttpGet]
        [Route("PeriodoMaiorUtilizacaoConjuntoElevadores")]
        public List<char> ObterPeriodoMaiorUtilizacaoConjuntoElevadores()
        {
            return _elevadorService.PeriodoMaiorUtilizacaoConjuntoElevadores();
        }


        [HttpGet]
        [Route("PercentualA")]
        public float ObterPercentualA()
        {
            return _elevadorService.PercentualDeUsoElevadorA();
        }

        [HttpGet]
        [Route("PercentualB")]
        public float ObterPercentualB()
        {
            return _elevadorService.PercentualDeUsoElevadorB();
        }

        [HttpGet]
        [Route("PercentualC")]
        public float ObterPercentualC()
        {
            return _elevadorService.PercentualDeUsoElevadorC();
        }

        [HttpGet]
        [Route("PercentualD")]
        public float ObterPercentualD()
        {
            return _elevadorService.PercentualDeUsoElevadorD();
        }

        [HttpGet]
        [Route("PercentualE")]
        public float ObterPercentualE()
        {
            return _elevadorService.PercentualDeUsoElevadorE();
        }
    }
}
