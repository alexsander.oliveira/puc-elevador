﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PucElevador.Dominio.Modelos
{
    public class DadoUsuarioElevador
    {
        [JsonPropertyName("andar")]
        public int Andar { get; set; }
        [JsonPropertyName("elevador")]
        public char Elevador { get; set; }
        [JsonPropertyName("turno")]
        public char Turno { get; set; }
    }
}
