﻿using PucElevador.Dominio.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace PucElevador.Dominio.Interfaces.Data
{
    public interface ILeitorArquivosData
    {
        List<DadoUsuarioElevador> LerArquivo();
    }
}
