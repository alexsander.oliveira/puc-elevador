﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PucElevador.Dominio.Interfaces.Data
{
    public interface IElevadorRepositorio
    {
        List<int> ObterAndarMenosUtilizados(int quantidade);
        List<char> ObterElevadorMaisFrequentado(int quantidade);
        List<char> ObterElevadorMenosFrequentado(int quantidade);
        List<char> ObterPeriodoDeMaiorFluxoDoMaisFrequentado(char elevador, int quantidade);
        List<char> ObterPeriodoDeMenorFluxoDoMenosFrequentado(char elevador, int quantidade);
        List<char> ObterPeriodoDeMaiorUtilizacao(int quantidade);
        float ObterPercentualDeUsoElevador(char elevador);
    }
}
