﻿using PucElevador.Dominio.Interfaces;
using PucElevador.Dominio.Interfaces.Data;
using System.Collections.Generic;
using System.Linq;

namespace PucElevador.Dominio.Servicos
{
    public class ElevadorService : IElevadorService
    {
        public readonly IElevadorRepositorio _elevadorRepositorio;
        public ElevadorService(IElevadorRepositorio elevadorRepositorio)
        {
            _elevadorRepositorio = elevadorRepositorio;
        }

        public List<int> AndarMenosUtilizado()
        {
            return _elevadorRepositorio.ObterAndarMenosUtilizados(10);
        }

        public List<char> ElevadorMaisFrequentado()
        {
            return _elevadorRepositorio.ObterElevadorMaisFrequentado(10);
        }

        public List<char> ElevadorMenosFrequentado()
        {
            return _elevadorRepositorio.ObterElevadorMenosFrequentado(10);
        }

        public float PercentualDeUsoElevadorA()
        {
            return _elevadorRepositorio.ObterPercentualDeUsoElevador('A');
        }

        public float PercentualDeUsoElevadorB()
        {
            return _elevadorRepositorio.ObterPercentualDeUsoElevador('B');
        }

        public float PercentualDeUsoElevadorC()
        {
            return _elevadorRepositorio.ObterPercentualDeUsoElevador('C');
        }

        public float PercentualDeUsoElevadorD()
        {
            return _elevadorRepositorio.ObterPercentualDeUsoElevador('D');
        }

        public float PercentualDeUsoElevadorE()
        {
            return _elevadorRepositorio.ObterPercentualDeUsoElevador('E');
        }

        public List<char> PeriodoMaiorFluxoElevadorMaisFrequentado()
        {
            var elevador = _elevadorRepositorio.ObterElevadorMaisFrequentado(1).First();
            return _elevadorRepositorio.ObterPeriodoDeMaiorFluxoDoMaisFrequentado(elevador, 10);
        }

        public List<char> PeriodoMaiorUtilizacaoConjuntoElevadores()
        {
            return _elevadorRepositorio.ObterPeriodoDeMaiorUtilizacao(10);
        }

        public List<char> PeriodoMenorFluxoElevadorMenosFrequentado()
        {
            var elevador = _elevadorRepositorio.ObterElevadorMenosFrequentado(1).First();
            return _elevadorRepositorio.ObterPeriodoDeMenorFluxoDoMenosFrequentado(elevador, 10);
        }
    }
}
