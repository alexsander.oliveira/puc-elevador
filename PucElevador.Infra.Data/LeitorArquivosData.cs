﻿using PucElevador.Dominio.Interfaces.Data;
using PucElevador.Dominio.Modelos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace PucElevador.Infra.Data
{
    public class LeitorArquivosData : ILeitorArquivosData
    {
        public List<DadoUsuarioElevador> LerArquivo()
        {
            var arquivo = File.ReadAllText("File/Input.json");
            var list = new List<DadoUsuarioElevador>();
            if (arquivo != null)
                list = JsonSerializer.Deserialize<List<DadoUsuarioElevador>>(arquivo);

            return list;
        }
        
    }
}
